import logo from './logo.svg';
import './App.css';

function App() {
  return (   
    <>
    <p class="left">Facebook helps you connect and share with the people in your life.</p>
  
    <div id="signin">
  <div class="form-title"></div>
  <div class="input-field">
    <input type="email" id="email" autocomplete="off"/>
    <i class="material-icons">email</i>
    <label for="email">Email address </label>
  </div>
  <div class="input-field">
    <input type="password" id="password"/>
    <i class="material-icons">lock</i>
    <label for="password">Password</label>
  </div>
  <a href="" class="forgot-pw">Forgot Password ?</a>
  <button class="login">Login</button>
  <div class="check">
    <i class="material-icons">check</i>
  </div>
</div>
<div id="gif">
  <a href="https://dribbble.com/shots/2197140-New-Material-Text-Fields">
    
  </a>
</div>
    </>
);
}

export default App;
